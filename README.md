# What you are #

A Full Stack Developer.(Front-End and Back-End(REST API's))

### What you know? ###

* Java core and Advance
* Javascript
    * jQuery 
    * Node.js
    * Express.js
    * Knowledge React.js
* Web Technology
    * HTML 
    * CSS3 
    * Bootstrap
    * AJAX
    * JSON
* SQL, Oracle 10g
* Nosql databases - Mongo and Rethink.
* Platform - Windows, Linux and Mac
* Versioning Tools - Github, Bitbucket.
* Cloud -AWS EC2 server management.

### Goal ###
To become a Lead Developer in JS(Fullstack)(In big 4)

### Preparation ###

* Javascript
    * Basics 
    * Objects
    * Closures
    * Callbacks
    * Promises
    * ES6
* React
    * React
    * Redux
    * React-native(Works for both android and IOS apps)
* Data Structure
* Algorithms ([In java](https://www.youtube.com/user/algorithmscourses/playlists?view=50&sort=dd&shelf_id=2))
* Networkin
    * Session 
    * Cookies 
    * HTTP and HTTPS
* Cloud Computing
* Python + Machine Learnning

## ToDO
    
* ABC (always be coding)
* Master at least one multi-paradigm language.(JAVA and Python)
* Know thy complexities.
* Re-invent the wheel.(Implement the most common data structures)
* Solve word problems. ([Check this](https://www.topcoder.com/community/data-science/data-science-tutorials/))
* Make coding easy :blush:

###['How to Get a Job at the Big 4 - Amazon, Facebook, Google & Microsoft'](https://www.youtube.com/watch?v=YJZCUhxNCv8)

## Target Companies

    *Quicker
    *Amazon
    *Flipcart
    *Accenture
    *
